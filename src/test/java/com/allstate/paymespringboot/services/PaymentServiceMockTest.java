package com.allstate.paymespringboot.services;

import com.allstate.paymespringboot.dao.PaymentRepository;
import com.allstate.paymespringboot.entities.Payment;
import com.allstate.paymespringboot.exceptions.OutOfRangeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)

public class PaymentServiceMockTest {

    @MockBean
    private PaymentRepository paymentRepo; //the MongoRepository is also a Bean now

    @Autowired
    PaymentService paymentService;

    @Test
    public void testFindByIdReturnsCustomer() throws OutOfRangeException {
        Payment payment = new Payment(2, new Date(), "Hourly", 55, 10);
        when(paymentRepo.findById(2)).thenReturn(Optional.of(payment)); //this basically means that
        // when encountering this lineof code, replace with this mock. for each statement to be replaced
        //  write what the mock replacement is

        assertNotEquals(Optional.empty(), paymentService.findById(2));
        verify(paymentRepo).findById(2); //verifies that statement was reached, again does not make the call
    }

}
