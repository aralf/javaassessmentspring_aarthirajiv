package com.allstate.paymespringboot.services;

import com.allstate.paymespringboot.entities.Payment;
import com.allstate.paymespringboot.exceptions.OutOfRangeException;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentServiceTest {

    @Autowired
    PaymentService paymentService;

    @BeforeAll
    public void setup() {
        // db created and atleast one record inserted
        Payment payment = new Payment("hourly", 11, 10);
        paymentService.save(payment);
        payment = new Payment("hourly", 22, 10);
        paymentService.save(payment);
    }

    @Test
    public void testFindByIdThrowsException() {
        Exception e = assertThrows(OutOfRangeException.class, () -> {
            Optional<Payment> payment = paymentService.findById(-1);
        });
    }


    @Test
    public void testFindByIdReturnsPayment() throws OutOfRangeException {
        Optional<Payment> payment = paymentService.findById(69); //pass valid Id
        assertNotEquals(Optional.empty(), payment);
    }

    @Test
    public void testSave() throws OutOfRangeException {
        Payment payment = new Payment("hourly", 55, 10);
        int savedId = paymentService.save(payment);
        Optional<Payment> paymentFromdb = paymentService.findById(savedId);//get newly inserted document
        assertEquals(savedId, paymentFromdb.get().getId());
    }

    @Test
    public void testFindByTypeReturnsList() {
        List<Payment> payments = paymentService.findByType("hourly");
        assertTrue(payments.size() >= 1);
    }

    @Test
    public void testRowCountAtleastPerSetup() { //see @BeforeAll setup() inserts 2 rows
        assertTrue(paymentService.rowCount() >= 2);
    }

    @Test
    public void testGetAllPayments() {
        List<Payment> payments = paymentService.getAllPayments();
        for(Payment p : payments) {
            System.out.println("Inside findByTypeTest "+p.getId()+" ** "+p.getType()+" ** "+p.getAmount());
        }
        assertTrue(payments.size() >= 2);
    }

    /*@Test
    public void testDeletedIdNotExists() {
        //
    }*/

    @AfterAll
    public void tearDown() {
        List<Payment> payments = paymentService.getAllPayments();
        for(Payment p: payments) {
            paymentService.delete(p.getId());
        }
    }
}
