package com.allstate.paymespringboot.controllers;

import com.allstate.paymespringboot.entities.Payment;
import com.allstate.paymespringboot.exceptions.OutOfRangeException;
import com.allstate.paymespringboot.services.PaymentMessaging;
import com.allstate.paymespringboot.services.PaymentService;
import com.allstate.paymespringboot.services.PaymentServiceImpl;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping ("/api/pay")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @Autowired
    private PaymentMessaging paymentMessaging;

    //Logger
    private static final Logger log = LoggerFactory.getLogger(PaymentController.class);

    @GetMapping("/status")
    public String getStatus() {
        log.info("PaymentController - /status endpoint accessed");
        return "Payment Service is running";
    }

    @PostMapping("/save")
    public String save(@RequestBody Payment payment) {
        paymentService.save(payment);
        String message= "A new Payment was added into Arti's Payment System with Id: "+payment.getId();
        String messageToSend = message + " [" + new Date() + "]";
        paymentMessaging.sendQueueMessage(message);
        return message;
    }

    @PostMapping("/update")
    public void update(@RequestBody Payment payment) {
       paymentService.update(payment);
    }

    //@DeleteMapping
    @PostMapping(value="/delete/{id}")
    public void delete(@PathVariable int id) {
        paymentService.delete(id);
    }

    @GetMapping("/getAll")
    public List<Payment> getAll()  {
        return paymentService.getAllPayments();
    }


    @GetMapping(value = "/findById/{id}")
    public Optional<Payment> find(@PathVariable int id) throws OutOfRangeException {
        return paymentService.findById(id);
    }

}