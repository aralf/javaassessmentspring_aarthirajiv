package com.allstate.paymespringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymespringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymespringbootApplication.class, args);
    }

}
