package com.allstate.paymespringboot.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PaymentMessaging {

    @Value("${queuename}")
    String queueName;
    @Value("${topicname}")
    String topicName;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendQueueMessage(String message) {
        String sendMessage = message + " [" + new Date() + " ]";
        jmsTemplate.convertAndSend(queueName, sendMessage);
    }

    public void sendTopicMessage(String message) {
        String messageString = message + " [" + new Date() + " ]";
        jmsTemplate.setPubSubDomain(true); //without this, it will assume Queue not Topic
        jmsTemplate.convertAndSend(topicName, messageString);
    }
}
