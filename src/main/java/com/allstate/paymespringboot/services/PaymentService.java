package com.allstate.paymespringboot.services;
import com.allstate.paymespringboot.entities.Payment;
import com.allstate.paymespringboot.exceptions.OutOfRangeException;

import java.util.List;
import java.util.Optional;

public interface PaymentService {

    public int save(Payment payment); //insert
    public Optional<Payment> findById(int id) throws OutOfRangeException;
    public List<Payment> findByType(String type);
    public int rowCount();
    public List<Payment> getAllPayments();
    public void delete(int id);
    public int update(Payment payment); // Repo save does insert and update
    //public void delete(Payment payment);
}
