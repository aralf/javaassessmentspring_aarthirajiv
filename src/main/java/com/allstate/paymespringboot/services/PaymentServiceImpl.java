package com.allstate.paymespringboot.services;

import com.allstate.paymespringboot.dao.PaymentRepository;
import com.allstate.paymespringboot.entities.Payment;
import com.allstate.paymespringboot.exceptions.OutOfRangeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    PaymentRepository paymentRepo;

    @Autowired
    SequenceGeneratorService sequenceGenerator;

    @Override
    public Optional<Payment> findById(int id) throws OutOfRangeException {
        if(id < 1)
            throw new OutOfRangeException("Id must be greater than 0 ", 1);
        return paymentRepo.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) {
        List<Payment> payments = paymentRepo.findByType(type);
        return payments;
    }

    @Override
    public int rowCount() {
        return (int)paymentRepo.count();
    }

    @Override
    public int save(Payment payment) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        payment.setId(sequenceGenerator.generateSequence(Payment.SEQUENCE_NAME));
        payment.setPaymentDate(new Date());
        return (paymentRepo.save(payment)).getId();
    }

    @Override
    public int update (Payment payment) {
        payment.setPaymentDate(new Date());
        return (paymentRepo.save(payment)).getId();
    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepo.findAll();
    }

    @Override
    public void delete(int id) {
        paymentRepo.deleteById(id);
    }

}
