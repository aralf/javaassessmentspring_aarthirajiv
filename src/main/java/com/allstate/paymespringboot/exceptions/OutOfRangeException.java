package com.allstate.paymespringboot.exceptions;
import java.util.Date;

public class OutOfRangeException extends Exception {
    private Date timestamp = new Date();
    private int severity;

    public OutOfRangeException(String message) {
        super(message);
    }

    public OutOfRangeException(String message, int severity) {
        super(message);
        this.severity = severity;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

}
