package com.allstate.paymespringboot.dao;
import com.allstate.paymespringboot.entities.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PaymentRepository extends MongoRepository<Payment, Integer> {

    //added as not default
    public List<Payment> findByType(String type);
}
