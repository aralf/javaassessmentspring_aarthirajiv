package com.allstate.paymespringboot.entities;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;
import java.util.Date;

@Document//(collection = "payments")
public class Payment {

    @Transient
    public static final String SEQUENCE_NAME = "payment_sequence";

    @Id
    //@Generated
    private int id;
    private Date paymentDate;

    private String type;
    private double amount;
    private int custId;

    public Payment(String type, double amount, int custId) {
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    public int getId() {
        return id;
    }

    public Payment() {
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", paymentDate=" + paymentDate +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", custId=" + custId +
                '}';
    }

    public Payment(Date paymentDate, String type, double amount, int custId) {
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    public Payment(int id, Date paymentDate, String type, double amount, int custId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPaymentDate() { return paymentDate;    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

}
